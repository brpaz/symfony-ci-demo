# Symfony Hello CI

> Sample project to demonstrate the creation of a fully CI / CD pipeline for a [Symfony](http://symfony.com) application using [GitLab](https://about.gitlab.com/) and [Heroku](https://heroku.com) for Deployment, using common tools and best practices.

## Motivation

The goal of this project is to demonstrate how to build a robust CI / CD Pipeline from scratch for a Symfony based project using standard tooling and best practices.

The most important part of this project are the concepts, code quality tools and stages. While GitLab and Heroku are great tools, you can easily use Circle CI or Kubernetes for example instead.

## Pre-Requisites

* A [GitLab](https://about.gitlab.com/) account
* An [Heroku](https://heroku.com) account

## Project Overview

This project is a simple Symfony project created with the Symfony command line tool, which I have added on top:

* Local development setup with [Docker](https://www.docker.com/) and[Docker Compose](https://docs.docker.com/compose/), including Xdebug and Blackfire support.
* GitLab CI pipeline.
* Deployment to Heroku, including the use of review apps.
* State of art PHP Quality tools included: 
  * [PHPUnit](https://phpunit.de/)
  * [psalm](https://github.com/vimeo/psalm)
  * [phpstan](https://github.com/phpstan/phpstan)
  * [Behat](https://behat.org/en/latest/) 
* Heroku Review Apps and automated deployments

## Install

After cloning this repository, run ```make setup```to bootsrap your project.

## Usage

There are two ways you can run this project locally. Using the Symfony cli tool or with Docker-Compose.

For symfony-cli tool:

```sh 
symfony serve
```

Make sure you have symfony cli tool installed on your system. [Check](https://symfony.com/doc/master/cloud/getting-started#installing-the-cli-tool) for details.

For Docker:

```sh
docker-compose up -d
```

## Contributing

Contributions, issues and Features requests are welcome.

## Show your support

<a href="https://www.buymeacoffee.com/Z1Bu6asGV" target="_blank"><img src="https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png" alt="Buy Me A Coffee" style="height: 41px !important;width: 174px !important;box-shadow: 0px 3px 2px 0px rgba(190, 190, 190, 0.5) !important;-webkit-box-shadow: 0px 3px 2px 0px rgba(190, 190, 190, 0.5) !important;" ></a>

## License 

Copywright @ 2019 [Bruno Paz](https://github.com/brpaz)

This project is [MIT](LLICENSE) Licensed.

<?php

namespace App\Tests\Unit\Controller;

use App\Controller\HealthController;
use PHPUnit\Framework\TestCase;

class HealthControllerTest extends TestCase
{
    public function testAdd() : void
    {
        $healthController = new HealthController();
        $response = $healthController->health();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals("{\"status\":\"OK\"}", $response->getContent());
    }
}

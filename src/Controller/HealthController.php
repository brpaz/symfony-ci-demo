<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class HealthController
{
    /**
     * @Route("/_health", name="app_health")
     */
    public function health() : JsonResponse
    {
        return new JsonResponse([
            'status' => 'OK'
        ]);
    }
}

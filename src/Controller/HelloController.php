<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HelloController
{
    /**
     * @Route("/", name="app_index")
     */
    public function health() : Response
    {
        return new Response("Hello from Symfony");
    }
}

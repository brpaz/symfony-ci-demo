
include .env
export

BIN_DIR=vendor/bin
DEV_ENV ?= local
CONTAINER_APP_NAME:="php

ifeq "$(DEV_ENV)" "docker"
	EXEC_PATH:=docker compose exec $(CONTAINER_APP_NAME)
endif

##
## ------------------
## Project setup
## ------------------
##
vendor: ## Installs vendor libraries
	${EXEC_CMD} composer install

start: ## Start the application server
ifeq "$(DEV_ENV)" "local"
		symfony serve
else
		docker-compose up -d
endif

stop: ## Stops the application server
ifeq "$(DEV_ENV)" "local"
		symfony stop
else
		docker-compose down
endif

##
## ------------------
## Project commands
## ------------------
##

setup: ## Setup the project
	@touch .env.local

console: ## Runs a command on Symfony console
ifeq "$(DEV_ENV)" "local"
		bin/console ${CMD}
else
		${COMPOSE_EXEC_PHP} bin/console ${CMD}
endif

sh: ## Opens console to the project when running with docker
	$(EXEC_PATH) sh

logs: ## Open logs when running the project with Docker
	docker-compose $(CONTAINER_APP_NAME) -f logs

##
## ------------------
## Quality
## ------------------
##

install: ## Installs composer dependencies
	@$(EXEC_PATH) composer install

analyze: ## Runs static analysis tools
	@$(EXEC_PATH) composer phpstan
	@$(EXEC_PATH) composer phpmd

security: ## Runs symfony security check to look for package known vulnerabilities
	@$(EXEC_PATH) composer security:check

lint: ## Run linter on configuration files
	@$(EXEC_PATH) composer lint

cs: ## Check for coding standards issues
	@$(EXEC_PATH) composer cs

cs-fix: ## Fix coding standards issues
	@$(EXEC_PATH) composer cs:fix

test: ## Run PHPunit tests
	@$(EXEC_PATH) composer test

test-functional: ## Run Behat Functional tests
	@$(EXEC_PATH) composer behat

docker-lint: ## Runs hadoint against application dockerfile
	docker run --rm -v "$(PWD):/code" -w "/code" hadolint/hadolint hadolint Dockerfile

help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.DEFAULT_GOAL := help
.PHONY: help setup vendor server start stop analyze lint cs cs-fix test test-functional
